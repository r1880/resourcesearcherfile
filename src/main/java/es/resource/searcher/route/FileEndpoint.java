package es.resource.searcher.route;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import es.resource.searcher.constants.FileConstants;
import es.resource.searcher.route.AnalyticEndpoint.ENDPOINTS;
import es.resource.searcher.service.FileService;
import es.valhalla.business.component.AbstractBaseHttpHandler;
import es.valhalla.constants.ValahallaConstants;
import es.valhalla.entities.ValhallaServiceRegistry;
import es.valhalla.exception.ValhallaBusinessExceptionEnum;
import es.valhalla.jwt.entities.JWTRoleENUM;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import jakarta.inject.Inject;

public class FileEndpoint extends AbstractBaseHttpHandler {

	@Inject
	FileService fileService;

	private static enum ENDPOINTS {
		STORE, DOWNLOAD, DELETE
	};

	private Map<String, List<ValhallaServiceRegistry>> serviceRegistry;

	private final static ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(FileEndpoint.class);

	public void storeDocument(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.POST.name(),
				FileConstants.SERVICE_PORT, FileConstants.SERVICE_PATH + "/:id", getSelfAddress());
		serviceRegistry.get(FileConstants.APP_NAME).add(self);
		final Route route = buildRouteMultipart(router, FileConstants.SERVICE_PATH + "/:id", HttpMethod.POST,
				MediaType.MULTIPART_FORM_DATA, FileConstants.DOCS_PATH);

		route.handler(ctx -> {
			LOGGER.info(INCOMING_REQUEST, ctx.request().absoluteURI(), ctx.request().method(), ctx.request().host());
			proccess(ctx, ENDPOINTS.STORE);

		}).failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));
	}

	public void downloadDocument(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.GET.name(),
				FileConstants.SERVICE_PORT, FileConstants.SERVICE_PATH + "/:id", getSelfAddress());
		serviceRegistry.get(FileConstants.APP_NAME).add(self);
		buildRouteEmpty(router, FileConstants.SERVICE_PATH + "/:id", HttpMethod.GET).handler(ctx -> {
			LOGGER.info(INCOMING_REQUEST, ctx.request().absoluteURI(), ctx.request().method(), ctx.request().host());
			final String filePath = fileService.downloadDocument(ctx.pathParam(FileConstants.ID));
			processSuccessFileResponse(ctx, filePath);

		});

	}

	public void deleteDocument(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.DELETE.name(),
				FileConstants.SERVICE_PORT, FileConstants.SERVICE_PATH + "/:id", getSelfAddress());
		serviceRegistry.get(FileConstants.APP_NAME).add(self);
		buildRouteEmpty(router, FileConstants.SERVICE_PATH + "/:id", HttpMethod.DELETE).handler(ctx -> {
			LOGGER.info(INCOMING_REQUEST, ctx.request().absoluteURI(), ctx.request().method(), ctx.request().host());

			proccess(ctx, ENDPOINTS.DELETE);
		});

	}

	private void proccess(final RoutingContext ctx, final ENDPOINTS endpoint) {
		final long init = System.currentTimeMillis();

		final String token = getTokenFromHeader(ctx.request());
		if (token != null) {
			final ValhallaServiceRegistry idpService = serviceRegistry.get(ValahallaConstants.AUTH_SERVICE).stream()
					.filter(s -> s.getServicePath().contains("validate")).findFirst().orElse(null);
			if (idpService == null) {
				LOGGER.error("IDP not registered");
				throw ValhallaBusinessExceptionEnum.IDP_NOT_REGISTERED.getException();
			}
			idpValidation(idpService.getServiceAddress(), idpService.getServicePath(), idpService.getPort(), token)
					.onSuccess(handler -> {
						if (handler.statusCode() == HttpResponseStatus.OK.code()) {
							final String serialized = executedOption(ctx, endpoint);
							if (endpoint.equals(ENDPOINTS.DOWNLOAD)) {
								processSuccessFileResponse(ctx, serialized);
							} else {
								processSuccessHttpResponse(ctx, serialized);
							}
						} else {
							processUnauthorizedResponse(ctx);
						}

					}).onFailure(handler -> {
						processUnauthorizedResponse(ctx);
					}).onComplete(finished -> LOGGER.info("Request processed in {} ms status code {}",
							System.currentTimeMillis() - init, finished.result().statusCode()));
		} else {
			processBadRequestResponse(ctx);
		}
	}

	private String executedOption(final RoutingContext ctx, final ENDPOINTS endpoint) {
		String serialized = null;
		switch (endpoint) {

		case STORE:
			final Map<String, Object> metadata = new HashMap<>();
			metadata.put("user", ctx.pathParam(FileConstants.ID));
			ctx.fileUploads().forEach(f -> {
				fileService.saveCV(metadata, f);
			});
			serialized = "";
			break;
		case DOWNLOAD:
						serialized=fileService.downloadDocument(ctx.pathParam(FileConstants.ID));
			break;
		case DELETE:
			fileService.
			break;

		}
		return serialized;
	}

	public void enableEndpoints(final Router router, final Map<String, List<ValhallaServiceRegistry>> serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
		serviceRegistry.put(FileConstants.APP_NAME, new ArrayList<>());
		storeDocument(router);
		downloadDocument(router);
		deleteDocument(router);

	}
}
