package es.resource.searcher.service;

import java.util.Map;

import io.vertx.ext.web.FileUpload;

public interface FileService {

	
	
	void saveCV(Map<String, Object> metadata, FileUpload file);

	String downloadDocument(String docId);

}
