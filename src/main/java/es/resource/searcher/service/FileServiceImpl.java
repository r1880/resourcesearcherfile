package es.resource.searcher.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import es.resource.searcher.constants.FileConstants;
import es.resource.searcher.entities.CV;
import es.resource.searcher.entities.Resource;
import es.valhalla.business.component.AbstractBusinessComponent;
import es.valhalla.constants.ValahallaConstants;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;
import io.vertx.ext.web.FileUpload;
import jakarta.annotation.PostConstruct;

public class FileServiceImpl extends AbstractBusinessComponent implements FileService {

	private final static ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(FileServiceImpl.class);
	private String url;
	private String databaseName;
	private String user;
	private String secret;
	private String bucketName;

	private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

	@PostConstruct
	private void init() {
		this.url = getStringProperty(FileConstants.APP_NAME, ValahallaConstants.MONGO_URL);
		this.databaseName = getStringProperty(FileConstants.APP_NAME, ValahallaConstants.MONGO_DBNAME);
		this.user = getStringProperty(FileConstants.APP_NAME, ValahallaConstants.MONGO_USER);
		this.secret = getStringProperty(FileConstants.APP_NAME, ValahallaConstants.MONGO_SECRET);
		this.bucketName = FileConstants.BUCKET_NAME;

	}

	@Override
	public void saveCV(final Map<String, Object> metadata, final FileUpload f) {
		Path source = Paths.get(f.uploadedFileName());
		Path target = Paths
				.get(getStringProperty(FileConstants.APP_NAME, FileConstants.DOCS_PATH).concat(f.fileName()));
		try {
			Files.move(source, target);
			File upload = target.toFile();
			final String docID = saveDocumentMongo(url, user, secret, databaseName, bucketName, metadata, upload);
			updateResource(metadata.get("user").toString(), f.fileName(), docID);
			Files.delete(target);
		} catch (IOException e) {
			LOGGER.error("Error processing file upload {}", e);
		}

	}
	
	public void deleteCV() {
		
		
	}

	@Override
	public String downloadDocument(final String docId) {
		final Map<String, byte[]> doc = getDocumentMongo(url, user, secret, databaseName, bucketName, docId);
		final AtomicReference<String> filePath = new AtomicReference<>();
		if (!doc.isEmpty()) {
			doc.forEach((k, v) -> {
				filePath.set(getStringProperty(FileConstants.APP_NAME, FileConstants.DOCS_PATH).concat(k));
				final File f = new File(filePath.get());
				try (final FileOutputStream fos = new FileOutputStream(f)) {
					fos.write(v);
				} catch (IOException e) {
					LOGGER.error("Error processing file download {}", e);

				}
			});

		}
		executor.schedule(() -> {
			try {
				Files.delete(Paths.get(filePath.get()));
			} catch (IOException e) {
				LOGGER.error("Error processing file delete {}", e);
			}
		}, 1, TimeUnit.MINUTES);
		return filePath.get();
	}

	private void updateResource(final String id, final String fileName, final String fileId) {
		final Resource r = getMongo(url, user, secret, databaseName, FileConstants.RESOURCES_COLLECTION, id,
				Resource.class);
		final List<CV> cvs = r.getCvs() != null ? r.getCvs() : new ArrayList<CV>();
		final CV cv = new CV();
		cv.setId(fileId);
		cv.setName(fileName);
		cvs.add(cv);
		r.setCvs(cvs);
		updateMongo(url, user, secret, databaseName, FileConstants.RESOURCES_COLLECTION, r, Resource.class);

	}
}
