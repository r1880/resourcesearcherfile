package es.resource.searcher.constants;

public class FileConstants {

	public static final String APP_NAME = "File";
	public static final String RESOURCES_COLLECTION = "resources";

	public static final String SERVICE_PATH = "/resourcesearcher/rest/service/file";
	public static final int SERVICE_PORT = 8089;
	public static final String COLLECTION_NAME = "mongo.collection";
	public static final String EMAIL_SUBJECT = "email.subject";
	public static final String EMAIL_FRONT_URL = "email.front.url";
	public static final String SERVICE_REGISTRY_PROP = "service.registry.path";
	public static final String DOCS_PATH = "files.tmp.path";
	public static final String BUCKET_NAME = "CVS";
	public static final String ID = "id";

	/** LOCAL PROPERTY **/
	public static final String DEFAULT_REGISTRY_PATH = "C:\\ResoureSearcher\\resource_searcher.json";

}
